import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  @Input() title = '';
  images = [
    {src: 'assets/img/pgr.png', alt: 'PGR'},
    {src: 'assets/img/glic.png', alt: 'GLIC'},
    {src: 'assets/img/gper.png', alt: 'GPER'},
    {src: 'assets/img/gperfundo.png', alt: 'GPERFUNDO'},
    {src: 'assets/img/monitor.png', alt: 'MONITOR'},
    {src: 'assets/img/glic.png', alt: 'GLIC'},
    {src: 'assets/img/gper.png', alt: 'GPER'},
    {src: 'assets/img/gperfundo.png', alt: 'GPERFUNDO'}
  ];

  @ViewChild('carousel', {static: false}) carousel: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  nextScroll() {
    this.carousel.nativeElement.scrollTo({left: this.carousel.nativeElement.scrollLeft + 800, behavior: 'smooth'});
  }

  prevScroll() {
    this.carousel.nativeElement.scrollTo({left: this.carousel.nativeElement.scrollLeft - 800, behavior: 'smooth'});
  }

}

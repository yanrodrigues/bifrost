import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { NavbarModule } from '../navbar/navbar.module';
import { CarouselModule } from '../carousel/carousel.module';



@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    NavbarModule,
    CarouselModule
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  pesquisa = '';

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    alert(this.pesquisa);
  }

  setPesquisa(valor) {
    this.pesquisa = valor;
  }

}
